//
//  MovieAppTests.swift
//  MovieAppTests
//
//  Created by Lars Dahl on 08.12.2016.
//  Copyright © 2016 Lars Dahl. All rights reserved.
//

import XCTest
import UIKit
import CoreData
@testable import MovieApp

class MovieAppTests: XCTestCase {
    
    var managedObjectContext : NSManagedObjectContext {
        return ModelManager.sharedManager.managedObjectContext
    }
    
    class func jsonDictionaryFromFile(_ filename: String) -> Dictionary<String, AnyObject> {
        let testBundle = Bundle(for: self)
        let path = testBundle.path(forResource: filename, ofType: "json")
        XCTAssertNotNil(path, "wrong filename")
        let data = try? Data(contentsOf: URL(fileURLWithPath: path!))
        XCTAssertNotNil(data, "wrong filename")
        do {
            if let jsonDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as? Dictionary<String,AnyObject> {
                return jsonDictionary
            }
            
        } catch let error {
            print(error)
        }
        return [String :AnyObject]()
    }
    
    class func stringToDate(date: String) -> NSDate {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yy"
        let formattedDate = dateFormatter.date(from: date)
        
        return formattedDate! as NSDate
    }
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let movies = Movie.allObjects(inManagedObjectContext: managedObjectContext)
        
        for movie in movies {
            managedObjectContext.delete(movie)
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCreateTwoMovies() throws {
        let _ = Movie.insertMovie("tt0816711", "World War Z", 2013, 7.0, "USA", "Action, Adventure, Horror", "116 min", nil, nil, NSDate(), inManagedObjectContext: managedObjectContext)
        
        let movie2 = Movie(context: managedObjectContext)
        movie2.imdbID = "tt0251413"
        movie2.title = "Star Wars"
        movie2.year = 1983
        movie2.imdbRating = 7.8
        movie2.genre = "Action, Adventure, Sci-Fi"
        movie2.runtime = "N/A"
        movie2.lastSeen = NSDate()
        
        try managedObjectContext.save()
        
        let fetchedMovies = Movie.allObjects(inManagedObjectContext: managedObjectContext)
        XCTAssertEqual(fetchedMovies.count, 2)
    }
    
    func testCreateAndDeleteMovie() throws {
        let movie1 = Movie.insertMovie("tt0816711", "World War Z", 2013, 7.0, "USA", "Action, Adventure, Horror", "116 min", nil, nil, NSDate(), inManagedObjectContext: managedObjectContext)
        
        var fetchedMovies = Movie.allObjects(inManagedObjectContext: managedObjectContext)
        XCTAssertEqual(fetchedMovies.count, 1)
        
        managedObjectContext.delete(movie1)
        
        fetchedMovies = Movie.allObjects(inManagedObjectContext: managedObjectContext)
        XCTAssertEqual(fetchedMovies.count, 0)
    }
    
    func testCreateAndUpdateMovie() throws {
        let movie1 = Movie.insertMovie("tt0816711", "World War Z", 2013, 7.0, "USA", "Action, Adventure, Horror", "116 min", nil, nil, NSDate(), inManagedObjectContext: managedObjectContext)
        
        var fetchedMovies = Movie.allObjects(inManagedObjectContext: managedObjectContext)
        XCTAssertEqual(fetchedMovies.count, 1)
        
        let movieToUpdate = fetchedMovies[0]
        XCTAssertEqual(movie1.imdbRating, movieToUpdate.imdbRating)
        
        // Update movie and save to DB
        movieToUpdate.imdbRating = 8.0
        
        managedObjectContext.refresh(movieToUpdate, mergeChanges: true)
        
        // Fetch movie and assert that IMDB-rating has been updated
        fetchedMovies = Movie.allObjects(inManagedObjectContext: managedObjectContext)
        XCTAssertEqual(fetchedMovies.count, 1)
        
        let updatedMovie = fetchedMovies[0]
        
        XCTAssertEqual(updatedMovie.imdbRating, movieToUpdate.imdbRating)
    }
    
    func testGetAverageImdbRating() {
        let movie1 = Movie.insertMovie("tt0816711", "World War Z", 2013, 7.0, "USA", "Action, Adventure, Horror", "116 min", nil, nil, NSDate(), inManagedObjectContext: managedObjectContext)
        let movie2 = Movie.insertMovie("tt0251413", "Star Wars", 1983, 7.8, "USA", "Action, Adventure, Sci-Fi", "N/A", nil, nil, NSDate(), inManagedObjectContext: managedObjectContext)
        
        let expectedAverageScore = (movie1.imdbRating + movie2.imdbRating) / 2
        let actualAverageScore = Movie.averageScore(predicate: nil)
        
        XCTAssertEqual(actualAverageScore, expectedAverageScore)
    }
    
    func testGetAverageImdbRatingWhenFilteredByYearAndLastSeen() {
        let movie1LastSeen = MovieAppTests.stringToDate(date: "20/12/2012")
        let movie2LastSeen = MovieAppTests.stringToDate(date: "19/06/2013")
        let movie3LastSeen = MovieAppTests.stringToDate(date: "24/12/2015")
        let _ = Movie.insertMovie("tt0816711", "World War Z", 2013, 6.9, "USA", "Action, Adventure, Horror", "116 min", nil, nil, movie1LastSeen, inManagedObjectContext: managedObjectContext)
        let movie2 = Movie.insertMovie("tt0251413", "Star Wars", 1983, 7.8, "USA", "Action, Adventure, Sci-Fi", "N/A", nil, nil, movie2LastSeen, inManagedObjectContext: managedObjectContext)
        let _ = Movie.insertMovie("tt2724064", "Sharknado", 2013, 3.3, "USA", "Horror, Sci-Fi", "86 min", nil, nil, movie3LastSeen, inManagedObjectContext: managedObjectContext)
        
        let minImdbRating: Float = 7.0
        let numberOfYears = -3
        let currentDate = Date()
        let calculatedDate = NSCalendar.current.date(byAdding: .year, value: numberOfYears, to: currentDate)!
        
        // Same predicate as used in MasterViewController to get movies
        // with better rating than 7.0 and last seen more than 3 years ago
        let predicate = NSPredicate(format: "(imdbRating > %lf) AND (lastSeen < %@)", minImdbRating, calculatedDate as NSDate)
        
        let expectedAverageScore = movie2.imdbRating
        let actualAverageScore = Movie.averageScore(predicate: predicate)
        
        XCTAssertEqual(actualAverageScore, expectedAverageScore)
    }
    
    func testCreateMovieFromInvalidJsonShouldReturnNil() {
        let invalidJsonAttributes = MovieAppTests.jsonDictionaryFromFile("1InvalidMovie")
        
        let movie = Movie(attributes: invalidJsonAttributes)
        
        XCTAssertNil(movie)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
