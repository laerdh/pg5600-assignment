//
//  DetailViewController.swift
//  MovieApp
//
//  Created by Lars Dahl on 08.12.2016.
//  Copyright © 2016 Lars Dahl. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var poster: UIImageView!
    @IBOutlet weak var lastSeenInput: UITextField!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var runtimeLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var imdbLabel: UILabel!
    @IBOutlet weak var lastSeenButton: UIButton!
    
    
    func configureView() {
        // Update the user interface for the detail item.
        if let detail = self.movieDetailItem {
            
            if let image = self.poster {
                if let actualImage = detail.poster {
                    let url = URL(string: actualImage)
                    
                    image.sd_setImage(with: url!, placeholderImage: UIImage(named: "Movie"))
                    image.contentMode = UIViewContentMode.scaleToFill
                }
            }
            
            if let title = self.titleLabel {
                if let actualTitle = detail.title {
                    title.text = actualTitle
                }
            }
            
            if let year = self.yearLabel {
                year.text = String(detail.year)
            }
            
            if let runtime = self.runtimeLabel {
                if let actualRuntime = detail.runtime {
                    runtime.text = actualRuntime
                }
            }
            
            if let genre = self.genreLabel {
                if let actualGenre = detail.genre {
                    genre.text = actualGenre
                }
            }
            
            if let imdb = self.imdbLabel {
                imdb.text = String(detail.imdbRating)
            }
            
            if let _ = self.lastSeenButton {
                if let actualLastSeen = detail.lastSeen {
                    updateLastSeenLabel(date: actualLastSeen)
                }
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Action

    @IBAction func deleteMovie(_ sender: AnyObject) {
        let deleteAlert = UIAlertController(title: "Slett", message: "Er du sikker på at du vil fjerne filmen fra favoritter?", preferredStyle: .alert)
        
        deleteAlert.addAction(UIAlertAction(title: "Ja", style: .default, handler: { (action: UIAlertAction!) in
            
            // Do delete
            self.deleteAction()
            
            // Pop viewcontroller
            if let navController = self.navigationController as UINavigationController? {
                navController.popViewController(animated: true)
            }
        }))
        
        deleteAlert.addAction(UIAlertAction(title: "Nei", style: .cancel, handler: nil))
        
        present(deleteAlert, animated: true, completion: nil)
    }
    
    @IBAction func addLastSeenDate(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Sist sett", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Lagre", style: .default, handler: { alert -> Void in
            
            let dateTextField = alertController.textFields![0] as UITextField
            
            // Save
            let date = dateTextField.text!
            self.saveLastSeenDate(date: date)
        })
        
        let cancelAction = UIAlertAction(title: "Avbryt", style: .cancel, handler: nil)
        
        alertController.addTextField { (textField: UITextField!) -> Void in
            textField.placeholder = "Sist sett dato (dag/måned/år)"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: Core Data functions
        
    func deleteAction() {
        // Save to DB
        let context = ModelManager.sharedManager.managedObjectContext
        context.delete(movieDetailItem!)
        
        do {
            try context.save()
        } catch let error {
            print("Error: \(error)")
        }
    }
    
    func saveLastSeenDate(date: String) {
        // Parse date
        let dateFormatter = self.getDateFormatter()
        let formattedDate = dateFormatter.date(from: date)
        self.movieDetailItem!.lastSeen = formattedDate as NSDate?
        
        // Merge changes to DB
        let context = ModelManager.sharedManager.managedObjectContext
        context.refresh(movieDetailItem!, mergeChanges: true)
        
        do {
            try context.save()
        } catch let error {
            print("Error \(error)")
        }
        
        // Update label
        let newDate = self.movieDetailItem!.lastSeen
        updateLastSeenLabel(date: newDate)
    }
    
    // MARK: Utility functions
    
    func getDateFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yy"
        
        return dateFormatter
    }
    
    func updateLastSeenLabel(date: NSDate?) {
        let dateFormatter = self.getDateFormatter()
        
        if let actualDate = date as Date? {
            let formattedDate = dateFormatter.string(from: actualDate)
            
            self.lastSeenButton.setTitle(formattedDate, for: .normal)
        }
    }

    var movieDetailItem: Movie? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }
}

