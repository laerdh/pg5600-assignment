//
//  Movie+CoreDataClass.swift
//  MovieApp
//
//  Created by Lars Dahl on 20.12.2016.
//  Copyright © 2016 Lars Dahl. All rights reserved.
//

import Foundation
import CoreData

@objc(Movie)
public final class Movie: NSManagedObject, Fetchable {
    
    static var entityName = "Movie"
    
    static func insertMovie(_ imdbID: String, _ title: String, _ year: Int16, _ imdbRating: Float, _ country: String?, _ genre: String?, _ runtime: String?, _ plot: String?, _ poster: String?, _ lastSeen : NSDate?, inManagedObjectContext managedObjectContext: NSManagedObjectContext) -> Movie {
        
        let movie = Movie(entity: NSEntityDescription.entity(forEntityName: entityName, in: managedObjectContext)!, insertInto: managedObjectContext)
        movie.imdbID = imdbID
        movie.title = title
        movie.year = year
        movie.timestamp = NSDate()
        movie.imdbRating = imdbRating
        movie.country = country
        movie.genre = genre
        movie.runtime = runtime
        movie.plot = plot
        movie.poster = poster
        movie.lastSeen = lastSeen
        
        return movie
    }
    
    static func averageScore(predicate: NSPredicate?, inManagedObjectContext managedObjectContext: NSManagedObjectContext = ModelManager.sharedManager.managedObjectContext) -> Float {
        var movies = [Movie]()
        
        if let actualPredicate = predicate {
            movies = Movie.allFilteredObjects(predicate: actualPredicate, inManagedObjectContext: managedObjectContext)
        } else {
            movies = Movie.allObjects(inManagedObjectContext: managedObjectContext)
        }
        
        var totalRating : Float = 0
        for movie in movies {
            totalRating += movie.imdbRating
        }
        
        return totalRating / Float(movies.count)
    }
    
    convenience init?(attributes: [String:AnyObject], inManagedObjectContext managedObjectContext: NSManagedObjectContext = ModelManager.sharedManager.managedObjectContext) {
        
        guard let actualId = attributes["imdbID"] as? String else {
            return nil
        }
        
        guard let actualTitle = attributes["Title"] as? String else {
            return nil
        }
        
        guard let actualYear = attributes["Year"] as? Int16 else {
            print("Could not unwrap year")
            return nil
        }
        
        guard let actualImdbRating = attributes["imdbRating"] as? Float else {
            print("Could not unwrap imdbRating")
            return nil
        }
        
        
        self.init(entity: NSEntityDescription.entity(forEntityName: Movie.entityName, in: managedObjectContext)!, insertInto: managedObjectContext)
        
        self.imdbID = actualId
        self.title = actualTitle
        self.year = actualYear
        self.imdbRating = actualImdbRating
        self.country = attributes["Country"] as? String
        self.genre = attributes["Genre"] as? String
        self.runtime = attributes["Runtime"] as? String
        self.plot = attributes["Plot"] as? String
        self.poster = attributes["Poster"] as? String
        self.timestamp = NSDate()
    }
}
