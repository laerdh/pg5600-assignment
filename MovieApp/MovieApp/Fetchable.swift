//
//  Fetchable.swift
//  MovieApp
//
//  Created by Lars Dahl on 20.12.2016.
//  Copyright © 2016 Lars Dahl. All rights reserved.
//

import Foundation
import CoreData

protocol Fetchable {
    
    static var entityName : String { get }
    
    static func allObjects(inManagedObjectContext managedObjectContext: NSManagedObjectContext) -> [Self]
    
    static func allFilteredObjects(predicate: NSPredicate, inManagedObjectContext managedObjectContext: NSManagedObjectContext) -> [Self]
}
