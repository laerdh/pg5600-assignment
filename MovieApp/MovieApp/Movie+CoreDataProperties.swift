//
//  Movie+CoreDataProperties.swift
//  MovieApp
//
//  Created by Lars Dahl on 20.12.2016.
//  Copyright © 2016 Lars Dahl. All rights reserved.
//

import Foundation
import CoreData


extension Movie {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Movie> {
        return NSFetchRequest<Movie>(entityName: "Movie");
    }

    @NSManaged public var country: String?
    @NSManaged public var genre: String?
    @NSManaged public var imdbID: String?
    @NSManaged public var imdbRating: Float
    @NSManaged public var lastSeen: NSDate?
    @NSManaged public var plot: String?
    @NSManaged public var poster: String?
    @NSManaged public var runtime: String?
    @NSManaged public var timestamp: NSDate?
    @NSManaged public var title: String?
    @NSManaged public var year: Int16

}
