//
//  Fetchable+DefaultImplementation.swift
//  MovieApp
//
//  Created by Lars Dahl on 20.12.2016.
//  Copyright © 2016 Lars Dahl. All rights reserved.
//

import Foundation
import CoreData

extension Fetchable where Self : NSManagedObject {
    
    static func allObjects(inManagedObjectContext managedObjectContext: NSManagedObjectContext) -> [Self] {
        let fetchRequest = NSFetchRequest<Self>(entityName: entityName)

        do {
            let results = try managedObjectContext.fetch(fetchRequest)
            return results
        } catch let error {
            print("Feil under innlasting av data fra CoreData: \(error)")
            return[Self]()
        }
    }
    
    static func allFilteredObjects(predicate: NSPredicate, inManagedObjectContext managedObjectContext: NSManagedObjectContext) -> [Self] {
        let fetchRequest = NSFetchRequest<Self>(entityName: entityName)
        fetchRequest.predicate = predicate
        
        do {
            let results = try managedObjectContext.fetch(fetchRequest)
            return results
        } catch let error {
            print("Feil under innlasting av filtrert data fra CoreData: \(error)")
            return [Self]()
        }
    }
}
