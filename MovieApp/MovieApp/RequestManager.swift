//
//  RequestManager.swift
//  MovieApp
//
//  Created by Lars Dahl on 08.12.2016.
//  Copyright © 2016 Lars Dahl. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire

class RequestManager {
    
    var searchResults = [JSON]()
    var movieDetails: JSON = JSON.null
    
    func search(title: String) {
        let url = "http://www.omdbapi.com/?s=\(title)&r=json"
        let urlStr = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let searchUrl = URL(string: urlStr)!
        
        print("URL: \(searchUrl.absoluteString)")
            
        Alamofire.request(searchUrl).responseJSON { response in
            if let results = response.result.value as? [String:AnyObject] {
                let items = JSON(results["Search"]).arrayValue
                self.searchResults += items
            } else {
                print("\nError?\n")
            }
            
            // Inform app that results has been fetched
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "searchResultsUpdated"), object: nil)
        }
    }
    
    func getMovieDetails(id: String) {
        let url = "http://www.omdbapi.com/?i=\(id)&plot=short&r=json"
        let searchUrl = URL(string: url)!
        
        Alamofire.request(searchUrl).responseJSON { response in
            if let result = response.result.value as? [String:AnyObject] {
                self.movieDetails = JSON(result)
            } else {
                print("\nError?\n")
            }
            
            // Inform app that result has been fetched
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateMovieDetails"), object: nil)
        }
    }
    
    func resetSearch() {
        searchResults.removeAll()
    }
}
