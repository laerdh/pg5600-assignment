//
//  MasterViewController.swift
//  MovieApp
//
//  Created by Lars Dahl on 08.12.2016.
//  Copyright © 2016 Lars Dahl. All rights reserved.
//

import UIKit
import CoreData

class MasterViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate {

    @IBOutlet weak var movieRatingView: MovieRatingView!
    @IBOutlet var mainView: UIView!
    
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var filterControl: UISegmentedControl!
    
    @IBOutlet weak var tableView: UITableView!
    
    let movieRatingViewDataSource = MovieRatingViewDataSourceController()
    
    let rowHeight: CGFloat = 60.0


    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        // Navigation bar
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = rowHeight
        
        // Set IMDB-rating view datasource
        movieRatingView.dataSource = movieRatingViewDataSource
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Add placeholder if table is empty
        checkEmptyTable(Array: self.fetchedResultsController.fetchedObjects!)
        
        // Reload IMDB average score data:
        movieRatingView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let movie = self.fetchedResultsController.object(at: indexPath)

                let controller = segue.destination as! DetailViewController
                controller.movieDetailItem = movie
            }
        }
    }
    
    // MARK: - Action
    
    @IBAction func filterChanged(_ sender: UISegmentedControl) {
        let minImdbRating: Float = 7.0
        let oldDate = calculateComparingDate()
        
        switch filterControl.selectedSegmentIndex
        {
            case 0:
                fetchFilteredMovies(predicate: nil)
            case 1:
                let predicate = NSPredicate(format: "(imdbRating > %lf) AND (lastSeen < %@)", minImdbRating, oldDate as NSDate)
                fetchFilteredMovies(predicate: predicate)
            default:
                break
        }
    }
    
    func fetchFilteredMovies(predicate: NSPredicate?) {
        // Set current predicate
        if let actualPredicate = predicate {
            self.fetchedResultsController.fetchRequest.predicate = actualPredicate
            self.movieRatingView.predicate = actualPredicate
        } else {
            self.fetchedResultsController.fetchRequest.predicate = nil
            self.movieRatingView.predicate = nil
        }
        
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            print("Feil under henting av filtrert data")
        }
        
        self.tableView.reloadData()
        self.movieRatingView.reloadData()
    }
    

    // MARK: - Table View

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        // Use own function to configurate cell
        let movie = self.fetchedResultsController.object(at: indexPath)
        configureCell(cell, withMovie: movie)
        
        return cell
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        //Return false if you do not want the specified item to be editable.
        return false
    }
    
    func configureCell(_ cell: UITableViewCell, withMovie movie: Movie) {
        if let actualUrl = movie.poster as String? {
            let url = URL(string: actualUrl)
            cell.imageView?.sd_setImage(with: url!, placeholderImage: UIImage(named: "Movie"))
            cell.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        }
        
        cell.selectionStyle = .none

        // Check and eventually format title length
        let movieTitle = formatMovieTitleString(title: movie.title!)
        let oldDate = calculateComparingDate()
    
        // If no last seen date, just set movie title and return
        guard let actualLastSeen = movie.lastSeen else {
            cell.textLabel!.text = movieTitle
            return
        }
        
        // Compare dates
        if movie.imdbRating > 7.0 && oldDate.compare(actualLastSeen as Date) == .orderedDescending {
            cell.textLabel!.text = "⭐️ \(movieTitle)"
        } else {
            cell.textLabel!.text = movieTitle
        }
    }
    
    // MARK: Utility functions

    func checkEmptyTable(Array: [NSManagedObject]) {
        if (Array.count == 0) {
            tableView.tableFooterView = UIView(frame: CGRect.zero)
            
            let label = UILabel()
            label.frame.size.height = tableView.frame.height
            label.frame.size.width = tableView.frame.width
            label.center = tableView.center
            label.center.y = tableView.frame.height / 3
            label.numberOfLines = 2
            label.textColor = UIColor.gray
            label.text = "Ingen filmer er lagt til i favoritter.\nTrykk på 'Legg til' for å legge til en film."
            label.textAlignment = .center
            label.tag = 1
            
            self.tableView.addSubview(label)
        } else {
            self.tableView.viewWithTag(1)?.removeFromSuperview()
        }
    }
    
    func formatMovieTitleString(title: String) -> String {
        if title.characters.count > 30 {
            let toIndex = title.index(title.startIndex, offsetBy: 30)
            return "\(title.substring(to: toIndex))..."
        }
        return title
    }
    
    func calculateComparingDate() -> Date {
        let numberOfYears = -3
        let currentDate = Date()
        let calculatedDate = NSCalendar.current.date(byAdding: .year, value: numberOfYears, to: currentDate)
    
        return calculatedDate!
    }
    
    // MARK: - Fetched results controller
    
    var fetchedResultsController: NSFetchedResultsController<Movie> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<Movie> = Movie.fetchRequest()
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "timestamp", ascending: false)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ModelManager.sharedManager.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return _fetchedResultsController!
    }
    var _fetchedResultsController: NSFetchedResultsController<Movie>? = nil
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            self.tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            self.configureCell(tableView.cellForRow(at: indexPath!)!, withMovie: anObject as! Movie)
        case .move:
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
}

