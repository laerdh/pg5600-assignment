//
//  MovieRatingViewDataSourceController.swift
//  MovieApp
//
//  Created by Lars Dahl on 20.12.2016.
//  Copyright © 2016 Lars Dahl. All rights reserved.
//

import UIKit

class MovieRatingViewDataSourceController : MovieRatingViewDataSource {
    
    @objc func averageImdbScoreInMovieRatingView(_ predicate: NSPredicate?, _ movieRatingView: MovieRatingView) -> Float {
        return Movie.averageScore(predicate: predicate)
    }
}
