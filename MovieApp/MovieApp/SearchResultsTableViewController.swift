//
//  SearchResultsTableViewController.swift
//  MovieApp
//
//  Created by Lars Dahl on 08.12.2016.
//  Copyright © 2016 Lars Dahl. All rights reserved.
//

import UIKit
import CoreData
import Foundation
import SwiftyJSON
import SDWebImage

class SearchResultsTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    
    var searchResults = [JSON]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    let entityName = "Movie"
    let id = "imdbID"
    
    let rowHeight: CGFloat = 70.0
    let searchController = UISearchController(searchResultsController: nil)
    let requestManager = RequestManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initialize the Search Bar
        initSearchBar()
        
        // Other configuration
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        tableView.rowHeight = rowHeight
        tableView.allowsMultipleSelection = true
        
        // Subscribe to notifications sent by RequestManager
        NotificationCenter.default.addObserver(self, selector: #selector(updateSearchResults), name: NSNotification.Name("searchResultsUpdated"), object:nil)
        NotificationCenter.default.addObserver(self, selector: #selector(addMovie), name: NSNotification.Name("updateMovieDetails"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        // Remove observers when view is not active
        NotificationCenter.default.removeObserver(self)
    }

    func updateSearchResults() {
        self.searchResults = requestManager.searchResults
        
        if self.searchResults.count < 1 {
            showMessage(title: "Info", message: "Ingen resultater")
        }
    }

    func addMovie() {
        let movieDetails = requestManager.movieDetails
        
        // Save to DB
        let context = ModelManager.sharedManager.managedObjectContext
        let newMovie = Movie(context: context)
        
        newMovie.imdbID = movieDetails[id].stringValue
        newMovie.title = movieDetails["Title"].stringValue
        newMovie.year = movieDetails["Year"].int16Value
        newMovie.imdbRating = movieDetails["imdbRating"].floatValue
        newMovie.country = movieDetails["Country"].stringValue
        newMovie.genre = movieDetails["Genre"].stringValue
        newMovie.plot = movieDetails["Plot"].stringValue
        newMovie.poster = movieDetails["Poster"].stringValue
        newMovie.runtime = movieDetails["Runtime"].stringValue
        newMovie.timestamp = NSDate()
        
        do {
            try context.save()
        } catch _ {
            showMessage(title: "Feil", message: "Klarte ikke å lagre film. Prøv igjen.")
        }
    }
    
    func initSearchBar() {
        searchController.searchBar.delegate = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.placeholder = "Søk på tittel"
        searchController.searchBar.searchBarStyle = .default
        searchController.searchBar.isTranslucent = false
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        searchController.searchBar.barTintColor = appDelegate.UIColorFromHex(rgbValue: 0x63B6BC)
        searchController.searchBar.tintColor = UIColor.white
        searchController.searchBar.backgroundImage = UIImage()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return searchResults.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath)
        cell.accessoryType = cell.isSelected ? .checkmark : .none
        cell.selectionStyle = .none
        
        // Let another function handle cell configuration
        configureCell(cell, withMovie: searchResults[indexPath.row])
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movie = searchResults[indexPath.row]
        let movieId = searchResults[indexPath.row][id].stringValue
        
        // Check cell
        if let cell = tableView.cellForRow(at: indexPath) {
            if cell.accessoryType == .checkmark {
                // Delete movie
                deleteMovie(withMovie: movie)
                
                // Uncheck
                cell.accessoryType = .none
            } else {
                // Add movie
                requestManager.getMovieDetails(id: movieId)
                
                // Check
                cell.accessoryType = .checkmark
            }
        }
        
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func configureCell(_ cell: UITableViewCell, withMovie movieJson: JSON) {
        
        // Get picture using SDWebImage
        if let actualUrl = movieJson["Poster"].stringValue as String? {
            print("URL: \(actualUrl)")
            
            let url = URL(string: actualUrl)
            cell.imageView?.sd_setImage(with: url!, placeholderImage: UIImage(named: "Movie"))
            cell.imageView?.contentMode = UIViewContentMode.scaleAspectFill
        }
        
        // Check if ID is found in DB. In case, set cell checked
        let context = ModelManager.sharedManager.managedObjectContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "timestamp", ascending: true)]
        fetchRequest.predicate = NSPredicate(format: "imdbID == %@", movieJson["imdbID"].stringValue)
        
        do {
            let exist = try context.count(for: fetchRequest)
            if exist > 0 {
                cell.accessoryType = .checkmark
            }
        } catch _ {
            showMessage(title: "Feil", message: "Problemer ved innlasting av filmer. Prøv igjen.")
        }
        
        // Set Cell label
        cell.textLabel?.text = movieJson["Title"].stringValue
        cell.detailTextLabel?.text = movieJson["Year"].stringValue
    }

    
    func deleteMovie(withMovie movie: JSON) {
        let context = ModelManager.sharedManager.managedObjectContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.predicate = NSPredicate(format: "imdbID == %@", movie["imdbID"].stringValue)
        fetchRequest.returnsObjectsAsFaults = false
        
        do {
            let fetchResult = try context.fetch(fetchRequest) as! [NSManagedObject]
            for movie in fetchResult {
                context.delete(movie)
            }
            
            try context.save()
        } catch _ {
            showMessage(title: "Feil", message: "Klarte ikke å slette film. Prøv igjen.")
        }
    }
    
    func showMessage(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(defaultAction)
        
        present(alert, animated: true, completion: nil)
    }
}

extension SearchResultsTableViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        requestManager.resetSearch()
        requestManager.search(title: searchBar.text!)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        requestManager.resetSearch()
        updateSearchResults()
    }
}
