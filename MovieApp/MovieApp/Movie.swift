//
//  Movie+CoreDataClass.swift
//  MovieApp
//
//  Created by Lars Dahl on 02.12.2016.
//  Copyright © 2016 Lars Dahl. All rights reserved.
//

import Foundation
import CoreData

@objc(Movie)
public class Movie: NSManagedObject {
    static var entityName = "Movie"
    
    
    static func insertMovie(_ title: String, _ year: Int16, _ imdbID: String?, _ poster: String?, inManagedObjectContext managedObjectContext: NSManagedObjectContext) -> Movie {
        
        let movie = Movie(entity: NSEntityDescription.entity(forEntityName: entityName, in: managedObjectContext)!, insertInto: managedObjectContext)
        movie.title = title
        movie.year = year
        movie.imdbID = imdbID
        movie.poster = poster
        
        return movie
    }
    
    convenience init?(attributes: [String: AnyObject], inManagedObjectContext managedObjectContext: NSManagedObjectContext = ModelManager.sharedManager.managedObjectContext) {
        
        guard let actualTitle = attributes["title"] as? String else {
            return nil
        }
        
        guard let actualYear = attributes["year"] as? Int16 else {
            return nil
        }
        
        self.init(entity: NSEntityDescription.entity(forEntityName: Movie.entityName, in: managedObjectContext)!, insertInto: managedObjectContext)
        
        self.title = actualTitle
        self.year = actualYear
        self.imdbID = attributes["imdbID"] as? String
        self.poster = attributes["poster"] as? String
    }
}
