//
//  MovieRatingView.swift
//  MovieApp
//
//  Created by Lars Dahl on 20.12.2016.
//  Copyright © 2016 Lars Dahl. All rights reserved.
//

import UIKit

@objc

protocol MovieRatingViewDataSource {
    
    func averageImdbScoreInMovieRatingView(_ predicate: NSPredicate?, _ movieRatingView: MovieRatingView) -> Float
}

class MovieRatingView: UIView {
    
    @IBOutlet weak var averageImdbScoreLabel : UILabel!
    
    var predicate: NSPredicate?
    
    weak var dataSource : MovieRatingViewDataSource? {
        didSet {
            reloadData()
        }
    }

    func reloadData() {
        if let averageScore = dataSource?.averageImdbScoreInMovieRatingView(predicate, self) {
            if (averageScore.isNaN) {
                averageImdbScoreLabel.text = "-"
            } else {
                averageImdbScoreLabel.text = String(format: "%.2f", averageScore)
            }
        } else {
            averageImdbScoreLabel.text = "-"
        }
    }
}
