# PG5600 - Innlevering, MovieApp
Dato: 20/12-2016
Navn: Lars Erling Westbye Dahl


# Biblioteker
- Alamofire
For async HTTP-kall

- SwiftyJSON
For å behandle JSON data

- SDWebImage
Asynkron bilde-nedlaster med cache-support.
Benyttes for å vise coverbilde i tablecell.


# Beskrivelse
- MasterViewController
Inneholder listen over filmer som er lagt til i favoritter med coverbilde. Dersom coverbilde ikke er tilgjengelig velges det et standard-bilde fra assets.
I denne oversikten kan man velge mellom å vise alle filmer eller filtrere på filmer som har høyere IMDB-rating enn 7.0 og sist sett dato eldre enn 3 år.
Filmene som er stjernemerket oppfyller kravene nevnt ovenfor.
Ved å trykke på en film blir du sendt til detaljesiden. Her får man mer informasjon om filmen og har mulighet til å oppdatere 'Sist sett'-dato. Dette gjøres ved å trykke på feltet med 'Sist sett' og taste inn dato på formatet dd/mm/yyyy i boksen (AlertControlleren) som dukker opp. Dette blir oppdatert i databasen umiddelbart.

- SearchResultsTableViewController
Appen lar deg søke i OMDB API etter ønskede filmer. API-kallet utføres ved hjelp av Alamofire. I første omgang henter den kun inn en liste med filmer i JSON-format og viser dette til brukeren.
Dersom brukeren ønsker å legge til en film fra listen over søketreff trykker vedkommende på den, deretter kjøres det et nytt kall mot APIet for å hente flere detaljer om filmen. I dette kallet brukes imdbID-attributtet i JSON-objektet for å hente detaljene om den spesifikke filmen.
Dersom søket ikke returnerer noen filmer får brukeren beskjed om dette. Dessverre er det her en liten bug som gjør at brukeren får samme beskjed dersom man angrer et søk ved å trykke 'Cancel'.

Ved å trykke på en film i søkeresultatene blir den markert med en hake og lagret i den lokale databasen på enheten. Neste gang man søker opp samme film vil den allerede var avhuket. Dersom man trykker på den igjen slettes den fra databasen og haken fjernes.

Siden API-kallene foregår asynkront benytter jeg NotificationCenter og setter nødvendige ViewControllere som observere - i dette tilfellet 'SearchResultsTableViewController'. Så fort det mottas en respons på et kall gis det et callback til abonnerende controller og controlleren kaller igjen på interne funksjoner for å behandle dataen som ble hentet.

- DetailViewController
Viser en detaljert oversikt over filmen med et større coverbilde. Brukeren har mulighet til å slette filmen herfra, men må bekrefte dette ved å trykke 'Ja'.
'Sist sett'-dato kan oppdateres ved å trykke på feltet 'Sist sett'. En boks med et felt for dato dukker opp og brukeren må skrive inn dato på formatet dd/mm/yyyy. Dette står også i placeholder-teksten.


Utility klasser:
- ModelManager
En hjelpeklasse for lettere tilgang til Core Data funksjoner

- RequestManager
Klassen for API-kall. Benytter Alamofire og SwiftyJSON.


Custom Views:
- MovieRatingView
Et custom view for å vise gjennomsnittlig IMDB-rating basert på filmene som vises.

- MovieRatingViewDataSource
Datakilden til MovieRatingView


Protokoller:
- Fetchable
Protokoll som brukes for å definere to nyttige metoder mot database, som å hente alle objekter eller objekter basert på et filter.
Implementeres av Movie-klassen.


# Tester
Testet hovedsaklig CRUD-operasjoner på Movie-objekter mot Core Data da appens funksjonalitet er veldig avhengig av at dette fungerer som det skal. For å sjekke en liten grad av robusthet testet jeg å opprette Movie-objekter med ugyldig JSON-data og sørget for at ikke dette gikk igjennom guardene i konstruktøren.
Testet også filtrerings-algoritmen da dette utgjør en stor del av brukeropplevelsen.

Dersom jeg hadde hatt mer tid ville jeg testet ViewControllerne, da spesielt MasterViewController og SearchResultsTableViewController da disse inneholder en god del logikk.
Ville også testet funksjonene som kaller eksternt API ifht feilsituasjoner. Dersom enheten er uten nett kan den kræsje i det den ikke klarer å hente inn coverbilde, siden dette ikke lagres på enheten.


# Kilder

StackOverflow (2015) How to add TextField to UIAlertController in swift. Tilgjengelig fra: http://stackoverflow.com/questions/31922349/how-to-add-textfield-to-uialertcontroller-in-swift (Hentet: 19 Desember 2016).

Theodoropoulos, G. (2015) A Beginner's Guide to NSDate in Swift. Tilgjengelig fra: https://www.appcoda.com/nsdate/ (Hentet: 19 Desember 2016)

StackOverflow (2011) A placeholder text to tableview if the datasource is empty. Tilgjengelig fra: http://stackoverflow.com/questions/5225726/a-placeholder-text-to-tableview-if-the-datasource-is-empty (Hentet: 18 Desember 2016)

Bogen, Håkon (2015) Demo-Students. Tilgjengelig fra https://github.com/haaakon/Demo-Students (Hentet: 18. Desember 2016)

Kenechi Learns Code (2016) UISearchController III (REST API Pagination, Swift 2.2). Tilgjengelig fra: https://www.youtube.com/watch?v=aPV9BzqNI-w (Hentet: 8. Desember 2016)
